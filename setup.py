# encoding: utf-8

from setuptools import setup, find_packages
from os.path import join, dirname

__version__ = '0.1.8.2017'

setup (
    name='poloniex-api-wrapper',
    version=__version__,
    description='Poloniex http Api python wrapper',
    license='MIT',
    author='Inconfig project',
    author_email='',
    packages=find_packages(),
    include_packages_data=True,
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    install_requires=[
        'requests>=2.10.0',
    ]

)