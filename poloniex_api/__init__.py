# encoding: utf-8

import requests

class BaseApi(object):
    """
        Base class for api wrappers
    """
    # Define request url for API, in this place is None because its not complete class
    url = None

    def get_request(self, request_options:dict=None) -> dict:
        """
            Send GET request using requests module.
        :request_options: dict
            Dictionary with request data
        :return: tuple(dict, boolean)
            Tuple object with request data and boolean result of operation
        """
        if request_options is None:
            request_options = {}

        if not isinstance(request_options, dict):
            raise TypeError(
                'Type of request_options is {0}, dict required'.format(type(request_options))
            )


        req = requests.get(
            url=self.url,
            params=request_options,
        )

        return req.json()


    def post_request(self, request_options:dict=None) -> dict:
        """
            Send POST request using request module
        :param request_options: dict
            Dictionary with request data
        :return: tuple(dict, boolean)
            Tuple object with request data and boolean result of operation
        """

        if request_options is None:
            request_options = {}

        if not isinstance(request_options, dict):
            raise TypeError(
                'Type of request_options is {0}, dict required'.format(type(request_options))
            )

        req = requests.post(
            url=self.url,
            json=request_options,
        )

        return req.json()
