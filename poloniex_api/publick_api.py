# encoding: utf-8

import time
from poloniex_api import BaseApi
from poloniex_api import exceptions

class PublickApi(BaseApi,object):
    """
        Wrapper class for poloniex publick api
    """
    url = 'https://poloniex.com/public'

    def __init__(self):
        """
            Init object, request special data for methods
        """
        self.pair_names = self.list_pairs_names()
        self.pair_ids = self.get_pairs_ids()

    def get_ticker(self) -> dict:
        """
            Returns the ticker for all markets
        :return: dict
            Dictionary with ticker data
        """
        return self.get_request(
            {'command': 'returnTicker'}
        )

    def get_24volume(self) -> dict:
        """
            Returns the 24-hour volume for all markets, plus totals for primary currencies.
        :return: dict
        """

        return self.get_request(
            {'command': 'return24hVolume'}
        )


    def get_orderbook(self, pair:str=None, depth:int=10) -> dict:
        """
            Returns the order book for a given market,
            as well as a sequence number for use with the Push API and an indicator
            specifying whether the market is frozen.
            You may set currencyPair to "all" to get the order books of all markets.
        :param pair: str
            CurrencyPair name
        :param depth: int
            Order book depth
        :return: dict
        """
        if pair is not None:
            if pair not in self.pair_names:
                raise exceptions.PairNameError(
                    'Unknown currency pair name {0}'.format(pair)
                )
        else:
            pair = 'all'

        return self.get_request(
            {
                'command': 'returnOrderBook',
                'currencyPair': pair,
                'depth': depth,
            }
        )


    def get_tradehistory(self, pair:str='BTC_ETH', start:float=None, end:float=time.time()) -> tuple:
        """
            Returns the past 200 trades for a given market,
            or up to 50,000 trades between a range specified in UNIX timestamps by the "start" and "end" GET parameters.
        :param pair: str
        :param start: float
        :param end: float
        :return: dict
        """
        if pair not in self.pair_names:
            raise exceptions.PairNameError(
                'Unknown currency pair name {0}'.format(pair)
            )

        if start is None:
            request_params = {
                'command': 'returnTradeHistory',
                'currencyPair': pair,
            }
        else:
            request_params = {
                'command': 'returnTradeHistory',
                'currencyPair': pair,
                'start': start,
                'end': end,
            }

        return  tuple(self.get_request(request_params))

    def list_pairs_names(self) -> tuple:
        """
            Get currency pairs names
        :return: list
            list pairs names
        """
        return tuple((self.get_ticker()).keys())

    def get_pairs_ids(self) -> dict:
        """
            Create and return dict like this {'id': 'pair name'}
        :return: dict
        """
        pairs_ids = dict()
        for p_name, p_vals in self.get_ticker().items():
            pairs_ids.update(
                {
                    p_vals['id']: p_name
                }
            )
        return pairs_ids
