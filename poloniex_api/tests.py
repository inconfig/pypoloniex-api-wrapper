# encoding: utf-8

import sys
import unittest
sys.path.append('./')

from poloniex_api.publick_api import PublickApi

class PublickApiTest(unittest.TestCase, object):
    """
        Tests for poloniex publock API wrapper
    """

    def setUp(self):
        """
            Create Poloniex API object
        """
        self.poloniex_api = PublickApi()

    def test_request_ticker(self):
        """
            Get ticker data from poloniex, assert type dict.
        """
        self.assertIsInstance(
            self.poloniex_api.get_ticker(), dict
        )

    def test_get_24Volume(self):
        """
            Get 24Volume data
        """
        self.assertIsInstance(
            self.poloniex_api.get_24volume(), dict
        )

    def test_get_orderbook(self):
        """
            Get order book data
        """
        self.assertIsInstance(
            self.poloniex_api.get_orderbook(), dict
        )

    def test_get_tradehistory(self):
        """
            Test getting trade history
        """
        self.assertIsInstance(
            self.poloniex_api.get_tradehistory(), tuple
        )

    def test_get_pairs_ids(self):
        """
            Test method get_pairs_ids
        """
        self.assertIsInstance(
            self.poloniex_api.get_pairs_ids(), dict
        )

if __name__ == '__main__':
    unittest.main()